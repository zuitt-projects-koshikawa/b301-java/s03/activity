import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Input an integer whose factorial will be computed:");
        Scanner input = new Scanner(System.in);
        int in = input.nextInt();

        if (in < 0) {
            try {
                throw new Exception("Cannot compute for factorials of negative numbers...");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            return;
        }

        int num = 1;
        try {

            for (int i = 1; i <= in; i++){
                num *= i;
            }

            String output = String.format("The factorial of %d is %d", in, num);
            System.out.println(output);
        } catch(Exception e){
            String errMsg = "";
            if (e.toString().equals("java.util.InputMismatchException")) {
                errMsg = "Invalid input, this program only accepts integer inputs";
            } else {
                errMsg = e.getMessage();
            }


            System.out.println(errMsg);
            e.printStackTrace();
        }
    }
}